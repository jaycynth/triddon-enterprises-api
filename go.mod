module github.com/jaycynth/triddon-enterprises

go 1.15

require (
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/cweill/gotests v1.5.3 // indirect
	github.com/gidyon/micro v1.4.4
	github.com/gidyon/micros v0.0.0-20200707081912-9d22591b4bed
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/grpc-gateway v1.15.0
	github.com/onsi/ginkgo v1.14.1
	github.com/onsi/gomega v1.10.2
	github.com/sirupsen/logrus v1.4.2
	github.com/speps/go-hashids v2.0.0+incompatible
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc
	google.golang.org/genproto v0.0.0-20200921165018-b9da36f5f452
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0
	gorm.io/gorm v1.20.1
)
