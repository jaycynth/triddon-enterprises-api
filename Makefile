PROJECT_NAME := triddon-enterprises
PROJECT_ROOT := ./ # Substitute wil relative path to project root
PKG := github.com/jaycynth/$(PROJECT_NAME)
SERVICE_CMD_FOLDER := ${PKG}/cmd
API_IN_PATH := api/proto
API_OUT_PATH := pkg/api
SWAGGER_DOC_OUT_PATH := api/swagger

setup_dev: ## Sets up a development environment for the project
	@cd deployments/compose &&\
	docker-compose up -d

setup_redis:
	@cd deployments/compose &&\
	docker-compose up -d redis

teardown_dev: ## Tear down development environment for the emrs project
	@cd deployments/compose &&\
	docker-compose down

protoc_car:
	@protoc -I=$(API_IN_PATH) -I=third_party --go_out=plugins=grpc:$(API_OUT_PATH)/carservice --go_opt=paths=source_relative car_service.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --grpc-gateway_out=logtostderr=true,paths=source_relative:$(API_OUT_PATH)/carservice car_service.proto
	@protoc -I=$(API_IN_PATH) -I=third_party --swagger_out=logtostderr=true:$(SWAGGER_DOC_OUT_PATH) car_service.proto

protoc_all: protoc_car

cp_doc:
	@cp -r $(SWAGGER_DOC_OUT_PATH)/ cmd/apidoc/dist/swagger/

gen_api_doc: protoc_all cp_doc

run_apidoc:
	@cd ./cmd/apidoc && go run *.go

start_apidoc: protoc_all cp_doc run_apidoc

run_gateway:
	cd ./cmd/gateway && make run

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
