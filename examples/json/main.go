package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var names []string // nil slice

	bs, err := json.Marshal(names)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(bs))
}
