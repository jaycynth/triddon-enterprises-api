package main

import (
	"fmt"
)

// Primitives: int,string,float32/float64
// Composite: struct{}, slices/arrays
// Reference: pointers, interfaces, maps

type secretMessage int32 // underlying type is int32

type hiddenMessage struct {
	msg string
} // underlying type is struct

func main() {
	sm := secretMessage(12)
	fmt.Printf("%T\n", sm) // secretMessage -> memory representation int32
	fmt.Printf("%v\n", sm) // secretMessage -> int32

	hm := hiddenMessage{msg: "hello mars"}
	fmt.Printf("%T\n", hm)
	fmt.Printf("%#v\n", hm)
}
