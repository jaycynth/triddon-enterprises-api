package main

import (
	"fmt"

	"github.com/speps/go-hashids"
)

func main() {
	hd := hashids.NewData()
	hd.Salt = "this is my salt"
	hd.MinLength = 30
	h, _ := hashids.NewWithData(hd)

	e, _ := h.Encode([]int{45})

	fmt.Println(e) // jyQ3p9aJEDngB0NVV05ev1WwPNxZq6 (service_id (uint))

	d, _ := h.DecodeWithError(e)

	fmt.Println(d) // [45] where(service_id>id)

	// 100 reslts
	// 10
	// 10 (90 remain)
	// next page token

	// where (id>10)

	// last id>90

	//
}
