package main

import (
	"context"
	"fmt"

	"github.com/gidyon/micro"
	"github.com/gidyon/micro/pkg/config"
	app_grpc_middleware "github.com/gidyon/micros/pkg/grpc/middleware"
	car_app "github.com/jaycynth/triddon-enterprises/internal/services/carservice"
	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"github.com/sirupsen/logrus"
)

func main() {

	fmt.Println("Start Executing..!")

	ctx := context.Background()

	// 1. Initialize config
	cfg, err := config.New(config.FromFile)
	handleErr(err)

	// 2. Create micro service
	carSrv, err := micro.NewService(ctx, cfg, nil)
	handleErr(err)

	// 3. Add custom options to service
	// Add Recovery middleware
	recoveryUIs, recoverySIs := app_grpc_middleware.AddRecovery()
	carSrv.AddGRPCUnaryServerInterceptors(recoveryUIs...)
	carSrv.AddGRPCStreamServerInterceptors(recoverySIs...)

	// Set base endpoint for runtime mux
	carSrv.SetServeMuxEndpoint("/")

	// Adding documentation handler
	// carSrv.AddEndpoint("/documentation", http.FileServer(http.Dir("./dist/")))

	// 4. Starts the service
	carSrv.Start(ctx, func() error {
		// API implemetation
		carAPI, err := car_app.NewCarAPI(&car_app.Options{
			SQLDB:  carSrv.GormDB(),
			Logger: carSrv.Logger(),
		})
		handleErr(err)

		// Register Car API implementation to gRPC server
		carservice.RegisterCarAPIServer(carSrv.GRPCServer(), carAPI)

		// Register grpc-gateway muxer with gRPC server over network connection
		handleErr(carservice.RegisterCarAPIHandler(ctx, carSrv.RuntimeMux(), carSrv.ClientConn()))

		return nil
	})

}

func handleErr(err error) {
	if err != nil {
		logrus.Fatalln(err)
	}
}
