package carservice

import (
	"fmt"
	"time"

	"github.com/Pallinder/go-randomdata"
	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// helper function for default value of car service
func fakeCarService() *carservice.CarService {
	return &carservice.CarService{
		Car: &carservice.Car{
			VehicleRegNo:     randomdata.Adjective(),
			OwnerPhoneNumber: randomdata.PhoneNumber(),
		},
		ServiceDate: time.Now().String()[:10],
		PaymentMode: carservice.PaymentMode(randomdata.Decimal(1, len(carservice.PaymentMode_name))),
		// ServiceType: carservice.ServiceType(randomdata.Decimal(1, len(carservice.ServiceType_name))),
		ServiceType: carservice.ServiceType_CAR_PARK,
		Service:     fakeCarPark(),
	}
}

func fakeCarWash() *carservice.CarService_CarWash {
	return &carservice.CarService_CarWash{
		CarWash: &carservice.CarWash{
			Amount: fmt.Sprint(randomdata.Number(300, 1500)),
		},
	}
}

func fakeCarPark() *carservice.CarService_CarPark {
	return &carservice.CarService_CarPark{
		CarPark: &carservice.CarPark{
			Amount: fmt.Sprint(randomdata.Number(50, 150)),
			ParkingType: carservice.ParkingType(
				randomdata.Decimal(1, len(carservice.ParkingType_value)),
			),
		},
	}
}

func fakeListCarService() *carservice.ListCarServicesResponse {
	return nil
}

var _ = Describe("Creating a car service", func() {

	var (
		ctx       context.Context
		createReq *carservice.CreateCarServiceRequest
	)

	BeforeEach(func() {
		// Initialize with default
		ctx = context.Background()
		createReq = &carservice.CreateCarServiceRequest{
			CarService: fakeCarService(),
		}
	})

	AfterEach(func() {})

	Describe("Creating a car service with malformed request", func() {
		// Specify()
		It("should fail when the request is nil", func() {
			createReq = nil
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)

			// 1. error must happen
			Expect(err).Should(HaveOccurred()) // Should or To

			// 2. grpc status code returned
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))

			// 3. res should be nil
			Expect(createRes).Should(BeNil())
		})

		It("should fail when car service is nil", func() {
			createReq.CarService = nil
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})

		It("should fail when car is nil", func() {
			createReq.CarService.Car = nil
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})

		It("should fail when service date is missing", func() {
			createReq.CarService.ServiceDate = ""
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})

		It("should fail when payment mode is unspecified", func() {
			createReq.CarService.PaymentMode = carservice.PaymentMode_PAYMENT_MODE_UNSPECIFIED
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})

		It("should fail when service type is unspecified", func() {
			createReq.CarService.ServiceType = carservice.ServiceType_SERVICE_TYPE_UNSPECIFIED
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})

		It("should fail when vehicle reg no is missing", func() {
			createReq.CarService.Car.VehicleRegNo = ""
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(createRes).Should(BeNil())
		})
	})

	Describe("Creating a car with well formed payload/request", func() {
		It("should succeed when the request has all mandatory fields", func() {
			createRes, err := CarServiceAPI.CreateCarService(ctx, createReq)
			Expect(err).ShouldNot(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.OK))
			Expect(createRes).ShouldNot(BeNil())
		})

	})

	// //Getting a car service request
	// Describe("Getting a car service: with malformed request", func() {
	// 	It("should fail when the request is nil", func() {
	// 		getReq = nil
	// 		getRes, err := CarServiceAPI.GetCarService(ctx, getReq)
	// 		Expect(err).Should(HaveOccurred())
	// 		Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 		Expect(getRes).Should(BeNil())
	// 	})

	// 	It("should fail when service id is missing", func() {
	// 		getRes, err := CarServiceAPI.GetCarService(ctx, getReq)
	// 		Expect(err).Should(HaveOccurred())
	// 		Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 		Expect(getRes).Should(BeNil())

	// 	})

	// })

	// //Updating a car service request
	// Describe("Updating a car service: malformed request", func() {
	// 	It("should fail when the request is nil", func() {
	// 		updateReq = nil
	// 		updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
	// 		Expect(err).Should(HaveOccurred())
	// 		Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 		Expect(updateRes).Should(BeNil())
	// 	})
	// })

	// It("should fail when car service is nil", func() {
	// 	updateReq.CarService = nil
	// 	updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
	// 	Expect(err).Should(HaveOccurred())
	// 	Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 	Expect(updateRes).Should(BeNil())
	// })

	// It("should fail when car service id is missing", func() {
	// 	updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
	// 	Expect(err).Should(HaveOccurred())
	// 	Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 	Expect(updateRes).Should(BeNil())

	// })

	// //List car services request
	// Describe("Get a list of car service: Malformed request", func() {
	// 	It("Should fail if request is nil", func() {
	// 		listReq = nil
	// 		listRes, err := CarServiceAPI.ListCarServices(ctx, listReq)
	// 		Expect(err).Should(HaveOccurred())
	// 		Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
	// 		Expect(listRes).Should(BeNil())
	// 	})
	// })

})
