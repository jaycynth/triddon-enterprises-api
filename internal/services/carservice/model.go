package carservice

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
)

// Service is a model for a Car
type Service struct {
	ServiceID        uint `gorm:"primaryKey;autoIncrement"` // sorting
	VehicleRegNo     string
	OwnerPhoneNumber string
	ServiceDate      string
	PaymentMode      string `gorm:"index;type:enum('CASH','MPESA')"`
	ServiceType      string
	ServiceData      []byte // bytes / mysql/sql database it can store data as bytes(json, blob), marshal or unmarshal is easier
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time
}

// GetCarPB creates a protobuf message from model instance
func GetCarPB(carServiceDB *Service) (*carservice.CarService, error) {
	servicePB := &carservice.CarService{
		ServiceId: fmt.Sprint(carServiceDB.ServiceID),
		Car: &carservice.Car{
			OwnerPhoneNumber: carServiceDB.OwnerPhoneNumber,
			VehicleRegNo:     carServiceDB.VehicleRegNo,
		},
		ServiceDate: carServiceDB.ServiceDate,
		PaymentMode: carservice.PaymentMode(carservice.PaymentMode_value[carServiceDB.PaymentMode]), // int32
		ServiceType: carservice.ServiceType(carservice.ServiceType_value[carServiceDB.ServiceType]),
		Service:     nil,
	}

	//
	switch servicePB.ServiceType {
	case carservice.ServiceType_SERVICE_TYPE_UNSPECIFIED:
	case carservice.ServiceType_CAR_PARK:
		carPark := &carservice.CarPark{}
		json.Unmarshal(carServiceDB.ServiceData, carPark)
	case carservice.ServiceType_CAR_WASH:
		carWash := &carservice.CarWash{}
		json.Unmarshal(carServiceDB.ServiceData, carWash)
	}

	return servicePB, nil
}

// GetCarDB creates a model from protobuf message
func GetCarDB(carSrcPB *carservice.CarService) (*Service, error) {

	// Always use getters to access message files
	carSrvDB := &Service{
		VehicleRegNo:     carSrcPB.GetCar().GetVehicleRegNo(),
		OwnerPhoneNumber: carSrcPB.GetCar().GetOwnerPhoneNumber(),
		ServiceDate:      carSrcPB.ServiceDate,
		PaymentMode:      carSrcPB.PaymentMode.String(),
		ServiceType:      carSrcPB.ServiceType.String(),
	}

	// Simply checking is there is data available in ServiceData field
	if len(carSrvDB.ServiceData) != 0 {
		// json marshal contents
		bs, err := json.Marshal(carSrvDB.ServiceData)
		if err != nil {
			return nil, fmt.Errorf("failed to unmarshal %w", err)
		}
		carSrvDB.ServiceData = bs
	}

	return carSrvDB, nil
}
