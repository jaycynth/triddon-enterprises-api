package carservice

import (
	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var _ = Describe("Listing car service @list", func() {
	var (
		listReq *carservice.ListCarServicesRequest
		ctx     context.Context
	)

	BeforeEach(func() {
		listReq = &carservice.ListCarServicesRequest{}
		ctx = context.Background()
	})

	Describe("Listing car services with malformed request", func() {
		It("should fail when the request is nil", func() {
			listReq = nil
			listRes, err := CarServiceAPI.ListCarServices(ctx, listReq)
			Expect(err).Should(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(listRes).Should(BeNil())
		})

		It("should fail when page token is incorrect", func() {
			listReq.PageToken = "incorect value"
			listRes, err := CarServiceAPI.ListCarServices(ctx, listReq)
			Expect(err).Should(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(listRes).Should(BeNil())
		})
	})

	Describe("Listing car service with well formed request", func() {
		Describe("Listing car servcies", func() {
			It("should succeed when everything is fine", func() {
				listRes, err := CarServiceAPI.ListCarServices(ctx, listReq)
				Expect(err).ShouldNot(HaveOccurred())
				Expect(status.Code(err)).Should(Equal(codes.OK))
				Expect(listRes).ShouldNot(BeNil())
			})
		})

		// Create 100 car service
		// List the car services using page size and update the page token until there are no next results

		Describe("Creating 100 car services", func() {
			It("should succeed when the request has all mandatory fields", func() {
				for i := 0; i < 100; i++ {
					createRes, err := CarServiceAPI.CreateCarService(ctx, &carservice.CreateCarServiceRequest{
						CarService: fakeCarService(),
					})
					Expect(err).ShouldNot(HaveOccurred())
					Expect(status.Code(err)).Should(Equal(codes.OK))
					Expect(createRes).ShouldNot(BeNil())
				}
			})
		})

		Describe("Testing pagination", func() {
			var pageToken, nextPageToken string
			It("should succeed when everything is fine", func() { // 1 spec and 20 iterations
				for nextPageToken != "" {
					listReq.PageToken = pageToken
					listRes, err := CarServiceAPI.ListCarServices(ctx, listReq)
					Expect(err).ShouldNot(HaveOccurred())
					Expect(status.Code(err)).Should(Equal(codes.OK))
					Expect(listRes).ShouldNot(BeNil())
					pageToken = listRes.NextPageToken
					nextPageToken = listRes.NextPageToken
				}
			})
		})
	})
})
