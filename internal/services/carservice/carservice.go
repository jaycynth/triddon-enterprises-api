package carservice

import (
	"errors"
	"fmt"
	"strconv"

	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"github.com/speps/go-hashids"

	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

// I have to atteend HR meeting its ongoing. Goodbye

type carserviceAPIServer struct {
	*Options // Embedding (used as type and name too)
	hasher   *hashids.HashID
}

// Options contains parameters for NewCarAPI
type Options struct {
	SQLDB      *gorm.DB
	Logger     grpclog.LoggerV2
	APIHashKey string
}

// NewCarAPI is a factory for creating carservice API singleton, //method built off of Server struct
func NewCarAPI(opt *Options) (carservice.CarAPIServer, error) {
	// Validation
	switch {
	case opt.SQLDB == nil:
		return nil, status.Error(codes.InvalidArgument, "nil database not allowed")
	case opt.Logger == nil:
		return nil, status.Error(codes.InvalidArgument, "nil Logger not allowed")
	case opt.APIHashKey == "":
		return nil, status.Error(codes.InvalidArgument, "missing API hask key")
	}

	// Creating list hasher
	hasher, err := NewHasherID(opt.APIHashKey)
	if err != nil {
		return nil, err
	}

	api := &carserviceAPIServer{
		Options: opt,
		hasher:  hasher,
	}

	// Auto migration
	err = api.SQLDB.AutoMigrate(&Service{})
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "failed to automigrate table")
	}

	return api, nil
}

// Black box(API + internals) and white box (API) testing
func (api *carserviceAPIServer) CreateCarService(
	ctx context.Context, createReq *carservice.CreateCarServiceRequest,
) (*carservice.CarService, error) {
	// Validation
	switch {
	case createReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case createReq.CarService == nil:
		return nil, status.Error(codes.InvalidArgument, "missing carservice")
	case createReq.GetCarService().GetCar() == nil:
		return nil, status.Error(codes.InvalidArgument, "missing car")
	case createReq.GetCarService().GetCar().GetVehicleRegNo() == "":
		return nil, status.Error(codes.InvalidArgument, "missing vehicle reg number")
	case createReq.GetCarService().GetServiceDate() == "":
		return nil, status.Error(codes.InvalidArgument, "missing date of entry")
	case createReq.GetCarService().GetPaymentMode() == carservice.PaymentMode_PAYMENT_MODE_UNSPECIFIED:
		return nil, status.Error(codes.InvalidArgument, "missing payment mode")
	case createReq.GetCarService().GetServiceType() == carservice.ServiceType_SERVICE_TYPE_UNSPECIFIED:
		return nil, status.Error(codes.InvalidArgument, "missing service type")
	}

	// Get the  carservice model
	carserviceDB, err := GetCarDB(createReq.CarService)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed to get model for carservice")
	}

	// Save the object to database
	err = api.SQLDB.Create(carserviceDB).Error
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "Failed save carservice")
	}

	// CANNOT ASSIGN USING GETTERS
	createReq.CarService.ServiceId = fmt.Sprint(carserviceDB.ServiceID)

	return createReq.CarService, nil
}

func (api *carserviceAPIServer) GetCarService(
	ctx context.Context, getReq *carservice.GetCarServiceRequest,
) (*carservice.CarService, error) {
	// Validation
	var serviceID uint
	switch {
	case getReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case getReq.ServiceId == "":
		return nil, status.Error(codes.InvalidArgument, "missing service id")
	default:
		id, err := strconv.Atoi(getReq.ServiceId) // ASCI to Integer
		if err != nil {
			// adc || 456.89
			return nil, status.Error(codes.InvalidArgument, "incorrect service id")
		}
		serviceID = uint(id)
	}

	carserviceDB := &Service{}

	err := api.SQLDB.First(carserviceDB, "service_id=?", serviceID).Error
	switch {
	case err == nil: // no error, sowe ignore
	case errors.Is(err, gorm.ErrRecordNotFound): // go 1.14
		return nil, status.Error(codes.NotFound, "car service does not exist")
	default:
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to get carservice") // Mearnt for developers
	}

	// Get carservice protobuf message
	carservicePB, err := GetCarPB(carserviceDB)
	if err != nil {
		api.Logger.Errorln(err)
		return nil, status.Error(codes.Internal, "failed to convert bank to message")
	}

	return carservicePB, nil
}

func (api *carserviceAPIServer) UpdateCarService(
	ctx context.Context, updateReq *carservice.UpdateCarServiceRequest,
) (*carservice.CarService, error) {
	// Validation
	switch {
	case updateReq == nil:
		return nil, status.Error(codes.InvalidArgument, "empty request")
	case updateReq.CarService == nil:
		return nil, status.Error(codes.InvalidArgument, "missing carservice")
	case updateReq.GetCarService().GetServiceId() == "":
		return nil, status.Error(codes.InvalidArgument, "missing service id")
	}

	// Get car service model
	carSrvDB, err := GetCarDB(updateReq.CarService)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	// 	CONDITION FIRST THEN THE UPDATE CLAUSE
	err = api.SQLDB.Where("service_id=?", updateReq.CarService.ServiceId).Updates(carSrvDB).Error
	if err != nil {
		api.Logger.Errorf("failed update: %v", err)
		return nil, status.Error(codes.Internal, "failed to update car service")
	}

	// Get updated car service
	api.SQLDB.First(carSrvDB, "service_id=?", updateReq.CarService.ServiceId)

	return GetCarPB(carSrvDB)
}

const defaultPageSize = 50

func (api *carserviceAPIServer) ListCarServices(
	ctx context.Context, listReq *carservice.ListCarServicesRequest,
) (*carservice.ListCarServicesResponse, error) {
	// Validation
	switch {
	case listReq == nil:
		return nil, status.Error(codes.InvalidArgument, "nil request")
	default:
	}

	var err error
	// Key pagination:
	// Primary key in wher clause
	// where(id>9950) limit(50) - very fast
	// Limitations: Primary key ama querying integer
	// comparison
	// sorting
	// [order by id DESC/ASC]

	// 3 millions logs (testing)
	// >100 ms

	// Hash functions
	// (input) => hash
	// (hash) => ?

	// (integer) => (string: hashed)
	// (string: hashed) => interger

	// hasher  *hashids.HashID

	// -1 -20 100000000

	pageSize := listReq.GetPageSize()
	if pageSize <= 0 || pageSize > defaultPageSize {
		pageSize = defaultPageSize
	}

	var serviceID uint
	pageToken := listReq.GetPageToken()
	if pageToken != "" {
		ids, err := api.hasher.DecodeInt64WithError(listReq.GetPageToken())
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to parse page token")
		}
		serviceID = uint(ids[0]) //95
	}

	carSrvDBs := make([]*Service, 0, pageSize) // 1 allocation

	// First request; pageToken="";serviceID=0;

	db := api.SQLDB.Limit(int(pageSize - 1)).Order("service_id DESC")

	if pageToken != "" {
		db = db.Where("service_id<?", serviceID) // Key pagination
	}

	// t1(1), t2(2), t3(3), t99(99)

	// Querying for many resources
	err = db.Find(&carSrvDBs).Error
	if err != nil {
		return nil, status.Error(codes.Internal, "faield to list resources from database")
	}

	carSrvPBs := make([]*carservice.CarService, 0, len(carSrvDBs)) // 1 allocation

	// Creating collection >50 && >0
	// [99, 98, 97, 96, 95, ...]
	// [94, 93, 92, 92,]

	for _, carSrvDB := range carSrvDBs {
		carSrvPB, err := GetCarPB(carSrvDB)
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		carSrvPBs = append(carSrvPBs, carSrvPB)
		serviceID = carSrvDB.ServiceID
	}
	// 95
	// 92

	// Scenario 1;
	// limit: 50
	// len(ussdLogsDB) == 50
	// means we have more results in next page

	// Scenario 2;
	// limit: 50
	// len(ussdLogsDB) == 40
	// means no results in next page

	// pageSize odd 50
	// limit(even) 49

	// we have 100 records
	// fetch 49 (rem 51)
	// fetch 49 (rem 2)
	// fetch 2 (rem 0)

	// Generating next page token
	var token string
	if int(pageSize) == len(carSrvDBs) {
		// Next page token
		token, err = api.hasher.EncodeInt64([]int64{int64(serviceID)})
		if err != nil {
			return nil, status.Error(codes.InvalidArgument, "failed to generate next page token")
		}
	}

	return &carservice.ListCarServicesResponse{
		NextPageToken: token,
		CarServices:   carSrvPBs,
	}, nil
}

func (api *carserviceAPIServer) FetchStatistics(ctx context.Context, fetchStatReq *carservice.FetchStatisticsRequest) (*carservice.Statistics, error) {
	return nil, status.Errorf(codes.Unimplemented, "method FetchStatistics not implemented")
}
