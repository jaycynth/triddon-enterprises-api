package carservice

import (
	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var _ = Describe("Getting a car @get", func() {
	var (
		getReq *carservice.GetCarServiceRequest
		ctx    context.Context
	)

	BeforeEach(func() {
		getReq = &carservice.GetCarServiceRequest{
			ServiceId: "0",
		}
		ctx = context.Background()
	})

	Describe("Geting a carservice with malformed request @get", func() {
		It("should fail when the request is nil", func() {
			getReq = nil
			getRes, err := CarServiceAPI.GetCarService(ctx, getReq)
			Expect(err).Should(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(getRes).Should(BeNil()) // more english more readable
		})

		It("should fail when car service id is missing", func() {
			getReq.ServiceId = ""
			getRes, err := CarServiceAPI.GetCarService(ctx, getReq)
			Expect(err).Should(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(getRes).Should(BeNil()) // more english more readable
		})
	})

	Describe("Getting a car service with well formed request", func() {
		carServicePB := fakeCarService()
		Context("Lets create a car service first", func() {
			testCreate(ctx, carServicePB)
		})

		// Get the created car
		It("it should get the created car", func() {
			getReq.ServiceId = carServicePB.ServiceId
			getRes, err := CarServiceAPI.GetCarService(ctx, getReq)
			Expect(err).ShouldNot(HaveOccurred())
			Expect(status.Code(err)).Should(Equal(codes.OK))
			Expect(getRes).ShouldNot(BeNil())
		})
	})
})
