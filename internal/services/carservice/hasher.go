package carservice

import "github.com/speps/go-hashids"

// NewHasherID creates a new hasher for decoding and encoding int64 slices
func NewHasherID(salt string) (*hashids.HashID, error) {
	hd := hashids.NewData()
	hd.Salt = salt
	hd.MinLength = 30

	return hashids.NewWithData(hd)
}
