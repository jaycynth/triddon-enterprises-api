package carservice

import (
	"github.com/jaycynth/triddon-enterprises/pkg/api/carservice"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var _ = Describe("Updating CarService @update", func() {
	var (
		updateReq *carservice.UpdateCarServiceRequest
		ctx       context.Context
	)

	BeforeEach(func() {
		updateReq = &carservice.UpdateCarServiceRequest{
			CarService: fakeCarService(),
		}
		ctx = context.Background()
	})

	Describe("updating car service with malformed request", func() {
		It("should fail when the request is nil", func() {
			updateReq = nil
			updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(updateRes).Should(BeNil())
		})
		It("should fail when car service is nil", func() {
			updateReq.CarService = nil
			updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(updateRes).Should(BeNil())
		})
		It("should fail when car service id is missing", func() {
			updateReq.CarService.ServiceId = ""
			updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
			Expect(err).Should(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.InvalidArgument))
			Expect(updateRes).Should(BeNil())
		})
	})

	Describe("Updating car service with well-formed request", func() {
		carServicePB := fakeCarService() // loal
		Context("Lets create a car service first", func() {
			testCreate(ctx, carServicePB)
		})

		// Then update service the car
		It("should succeed when everything is fine", func() {
			updateReq.CarService.ServiceId = carServicePB.ServiceId
			updateRes, err := CarServiceAPI.UpdateCarService(ctx, updateReq)
			Expect(err).ShouldNot(HaveOccurred()) // Should or To
			Expect(status.Code(err)).Should(Equal(codes.OK))
			Expect(updateRes).ShouldNot(BeNil())

			// Some assertions to affirm the resource was updated
			Expect(carServicePB.Car.VehicleRegNo).ShouldNot(Equal(updateRes.Car.VehicleRegNo))
		})
	})
})

func testCreate(ctx context.Context, carservicePB *carservice.CarService) {
	It("should succeed when the request has all mandatory fields", func() {
		createRes, err := CarServiceAPI.CreateCarService(ctx, &carservice.CreateCarServiceRequest{
			CarService: carservicePB,
		})
		Expect(err).ShouldNot(HaveOccurred())
		Expect(status.Code(err)).Should(Equal(codes.OK))
		Expect(createRes).ShouldNot(BeNil())
	})
}
